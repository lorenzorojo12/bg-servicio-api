**Instrucciones de Instalación:**

- Tener la version de PHP 8.0.2 o superior.
- Ejecutar el comando: composer install
- Luego tendriamos que crear la base de datos y agregar el nombre de la misma, usuario y contraseña al archivo .env
- Una vez tengamos conexion existosa,  debemos ejecutar el comando: php artisan migrate y luego ejecutar el comando: php artisan db:seed (Para generar los usuarios)
- Luego ejecutar el comando: php artisan serve y se ejecutara el proyecto por el puerto 8000 de localhost
- Las rutas de la aplicación son las siguientes:
    - api/v1/users
    - api/v1/users/{id}

Documentación Online en POSTMAN:
[https://documenter.getpostman.com/view/18208862/2s8YessBx6](https://documenter.getpostman.com/view/18208862/2s8YessBx6 "smartCard-inline")
