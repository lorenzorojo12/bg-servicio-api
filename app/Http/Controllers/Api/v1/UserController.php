<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\UserResource;
use App\Models\LogUser;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Collection
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function show($id)
    {
        $user = User::find($id) ;
        // Si exite el usuario
        if($user){
            // Crear el nuevo registro del log
            $log = new LogUser();
            $log->description = "Visualizando detalle del usuario";
            $log->user_id = $id;
            $log->save();

            // Creo el recurso con los datos completos
            $data = new UserResource(User::with(['log_users'])->find($id));

            // Retornar respuesta
            return response()->json(['result' => true, 'message' => 'User exists', 'data' => $data], 200);
        }
        return response()->json(['message' => 'User not found', 'errors' => $user], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return Response
     */
    public function destroy(User $user)
    {
        //
    }
}
